Source: golang-golang-x-tools
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Martina Ferrari <tina@debian.org>,
           Michael Stapelberg <stapelberg@debian.org>,
           Tim Potter <tpot@hpe.com>,
           Anthony Fok <foka@debian.org>,
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-golang,
               golang-any (>= 2:1.19~),
               golang-github-google-go-cmp-dev (>= 0.6.0),
               golang-github-yuin-goldmark-dev (>= 1.4.13),
               golang-golang-x-mod-dev (>= 0.19.0),
               golang-golang-x-net-dev (>= 1:0.27.0),
               golang-golang-x-sync-dev (>= 0.7.0),
Testsuite: autopkgtest-pkg-go
Standards-Version: 4.7.2
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-golang-x-tools
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-golang-x-tools.git
Homepage: https://golang.org/x/tools
XS-Go-Import-Path: golang.org/x/tools

Package: golang-golang-x-tools
Architecture: any
Depends: libjs-jquery,
         libjs-jquery-ui,
         ${misc:Depends},
         ${shlibs:Depends},
Recommends: golang-doc,
Static-Built-Using: ${misc:Static-Built-Using},
Description: supplementary Go tools
 This subrepository holds the source for various packages and tools that
 support the Go programming language.
 .
 Some of the tools, godoc and vet for example, used to be included in the
 golang-go package. Others, including the Go oracle and the test coverage tool,
 can be fetched with "go get".
 .
 Packages include a type-checker for Go and an implementation of the Static
 Single Assignment form (SSA) representation for Go programs.

Package: golang-golang-x-tools-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-github-yuin-goldmark-dev (>= 1.4.13),
         golang-golang-x-mod-dev (>= 0.19.0),
         golang-golang-x-net-dev (>= 1:0.27.0),
         golang-golang-x-sync-dev (>= 0.7.0),
         ${misc:Depends},
Description: supplementary Go tools (development files)
 This subrepository holds the source for various packages and tools that
 support the Go programming language.
 .
 Some of the tools, godoc and vet for example, used to be included in the
 golang-go package. Others, including the Go oracle and the test coverage tool,
 can be fetched with "go get".
 .
 Packages include a type-checker for Go and an implementation of the Static
 Single Assignment form (SSA) representation for Go programs.
 .
 This package contains the development files.
