#!/usr/bin/make -f
# -*- makefile -*-

# Excluded binaries. List all binary names:
# go list -f '{{ if eq .Name "main" }}{{ .ImportPath }}{{ end }}' ./...
# (cd gopls && go list -f '{{ if eq .Name "main" }}{{ .ImportPath }}{{ end }}' ./...)
export DH_GOLANG_EXCLUDES := \
	cmd/signature-fuzzer \
	go/analysis/passes/httpmux/cmd/httpmux \
	gopls \
	internal/stack/gostacks \
	# EOL

# Excluded tests
export DH_GOLANG_EXCLUDES += \
	copyright \
	# EOL

# needed for tests.
export DH_GOLANG_INSTALL_EXTRA := \
	godoc/static \
	# EOL

# eg #753978
# stress #793693
# bundle #818551
RENAME_BIN := \
	bisect \
	bundle \
	deadcode \
	eg \
	stress \
	# EOL

# All go/analysis/*/*/cmd/*
RENAME_BIN += \
	defers \
	fieldalignment \
	findcall \
	ifaceassert \
	lostcancel \
	nilness \
	shadow \
	stringintconv \
	unmarshal \
	unusedresult \
	# EOL

# All cmd/auth/*
RENAME_BIN += \
	authtest \
	cookieauth \
	gitauth \
	netrcauth \
	# EOL

# All go/*/internal/*
RENAME_BIN += \
	nodecount \
	play \
	# EOL

execute_before_dh_auto_configure:
	# Copy golang.org/x/telemetry for opt-in transparent telemetry;
	# see https://go.dev/issue/58894
	mkdir -p _build
	cp -av debian/go/src _build/

execute_after_dh_auto_install:
	for i in $(RENAME_BIN); \
	do \
	    mv debian/tmp/usr/bin/$$i debian/tmp/usr/bin/golang-$$i; \
	done
	# gopls can't be not imported by other packages.
	# remove it to avoid tests in autopkgtest which causes circular
	# dependencies when updating golang-golang-x-vuln.
	rm -rvf debian/tmp/usr/share/gocode/src/golang.org/x/tools/gopls

override_dh_auto_test:
ifeq (,$(findstring gccgo, $(shell go version)))
	# use short tests where possible (especially to skip tests like
	# TestWebIndex which are very slow)
	dh_auto_test -- -short -timeout=30m
endif

%:
	dh $@ --builddirectory=_build --buildsystem=golang
